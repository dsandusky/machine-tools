﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; SQL Server Management Studio
SetTitleMatchMode, REGEX
#IfWinActive, SQL
::ssf::SELECT * FROM
::s1f::SELECT 1 FROM
::del::DELETE FROM
::trun::TRUNCATE TABLE
::colvar::DECLARE @ColCd INT = -1`nDECLARE @ColNd CHAR(2) = 'WH'`nDECLARE @ShopId INT = 45`nDECLARE @Brand CHAR(2) = 'GL'
::s10f::SELECT TOP 10 * FROM
::s100f::SELECT TOP 100 * FROM
#SingleInstance force